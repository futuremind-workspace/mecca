﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MECCA.Core.Expresssion
{
    internal sealed class ParameterReplacementVistor : ExpressionVisitor
    {
        private Dictionary<ParameterExpression, ParameterExpression> _map;

        public Expression ReplaceParameter<T>(Expression<T> expression, Expression<T> expressionToReplace)
        {
            _map = expression
                .Parameters
                .Select((f, i) => new { f, s = expressionToReplace.Parameters[i] })
                .ToDictionary(p => p.s, p => p.f);

            return Visit(expressionToReplace.Body);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            ParameterExpression replacement;

            if (_map.TryGetValue(node, out replacement))
            {
                node = replacement;
            }

            return base.VisitParameter(node);
        }
    }
}
