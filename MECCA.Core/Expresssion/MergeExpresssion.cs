﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MECCA.Core.Expresssion
{
    public static class MergeExpresssion
    {
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
        {
            return MergeExpression(expression1, expression2, Expression.And);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
        {
            return MergeExpression(expression1, expression2, Expression.Or);
        }

        public static Expression<Func<TM, bool>> Any<TM, TS>(this Expression<Func<TM, IEnumerable<TS>>> expressionCollection, Expression<Func<TS, bool>> subExpression)
        {
            var callAny = Expression.Call(
                typeof(Enumerable),
                "Any",
                new[] { typeof(TS) }, expressionCollection.Body, subExpression);

            return Expression.Lambda<Func<TM, bool>>(callAny, expressionCollection.Parameters);
        }

        private static Expression<Func<T, bool>> MergeExpression<T>(Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2, Func<Expression, Expression, Expression> mergeExpression)
        {
            if (expression2 == null) return expression1;
            var replacedExpression = new ParameterReplacementVistor().ReplaceParameter(expression1, expression2);
            return Expression.Lambda<Func<T, bool>>(mergeExpression(expression1.Body, replacedExpression), expression1.Parameters);
        }
    }
}
