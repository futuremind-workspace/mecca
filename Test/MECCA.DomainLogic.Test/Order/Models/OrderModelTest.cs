﻿using System;
using System.Collections.Generic;
using MECCA.DomainEntities.ValueObjects.Order;
using MECCA.DomainLogic.Order.Models;

namespace MECCA.DomainLogic.Test.Order.Models
{
    public class OrderModelTest
    {
        public OrderModel Model(OrderType orderType)
        {
            return new OrderModel
            {
                Id = Guid.NewGuid(),
                OrderNumber = "0001",
                OrderDate = DateTime.UtcNow,
                Total = 10.0M,
                ChannelId = orderType == OrderType.Online ? 1 : (int?)null,
                StoreId = orderType == OrderType.Store ? 1 : (int?)null,
                OrderType = orderType,
                LineItems = new List<LineItemModel>
                {
                    (new LineItemModelTest()).Model()
                }
            };
        }
    }
}
