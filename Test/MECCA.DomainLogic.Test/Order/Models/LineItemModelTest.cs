﻿using System;
using MECCA.DomainLogic.Order.Models;

namespace MECCA.DomainLogic.Test.Order.Models
{
    public class LineItemModelTest
    {
        public LineItemModel Model()
        {
            return new LineItemModel
            {
                Id = Guid.NewGuid(),
                OrderId = Guid.NewGuid(),
                ItemCode = "0001",
                Quantity = 10,
                Price = 10.0M
            };
        }
    }
}
