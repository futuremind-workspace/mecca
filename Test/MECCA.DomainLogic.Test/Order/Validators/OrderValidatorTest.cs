﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using FluentValidation.TestHelper;
using MECCA.DomainEntities.ValueObjects.Order;
using MECCA.DomainLogic.Order.Models;
using MECCA.DomainLogic.Order.Validators;
using MECCA.DomainLogic.Test.Order.Models;
using NUnit.Framework;

namespace MECCA.DomainLogic.Test.Order.Validators
{
    [TestFixture]
    public class OrderValidatorTest
    {
        private OrderValidator _validator;

        [OneTimeSetUp]
        public void SetUp()
        {
            _validator = new OrderValidator();
        }

        [Test]
        public void Should_Not_Have_Validation_Errors_When_ValidModel_Supplied()
        {
            var model = (new OrderModelTest()).Model(OrderType.Online);
            _validator.Validate(model).IsValid.Should().BeTrue();
        }

        [Test]
        public void Should_Have_Error_When_OrderNumber_Is_Empty()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.OrderNumber, new OrderModel
            {
                OrderNumber = null
            });
        }

        [Test]
        public void Should_Not_Have_Error_When_OrderNumber_Is_Empty()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.OrderNumber, new OrderModel
            {
                OrderNumber = "TestOrderNumber"
            });
        }

        [Test]
        public void Should_Have_Error_When_Order_Date_Is_Invalid()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.OrderDate, new OrderModel
            {
                OrderDate = DateTime.UtcNow.AddDays(3)
            });
        }

        [Test]
        public void Should_Not_Have_Error_When_Order_Date_Is_Valid()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.OrderDate, new OrderModel
            {
                OrderDate = DateTime.UtcNow
            });
        }

        [Test]
        public void Should_Have_Error_When_Channel_Id_Is_Null_For_Online_Order()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.ChannelId, new OrderModel
            {
                ChannelId = null,
                OrderType = OrderType.Online
            });
        }

        [Test]
        public void Should_Not_Have_Error_Channel_Id_Is_Not_Null_For_Online_Order()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.OrderDate, new OrderModel
            {
                ChannelId = 1,
                OrderType = OrderType.Online
            });
        }

        [Test]
        public void Should_Have_Error_When_Store_Id_Is_Null_For_Store_Order()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.StoreId, new OrderModel
            {
                StoreId = null,
                OrderType = OrderType.Store
            });
        }

        [Test]
        public void Should_Not_Have_Error_Store_Id_Is_Not_Null_For_Store_Order()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.StoreId, new OrderModel
            {
                StoreId = 1,
                OrderType = OrderType.Store
            });
        }

        [Test]
        public void Should_Have_Error_When_Total_Is_Invalid()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.Total, new OrderModel
            {
                Total = 10.0M,
                LineItems = new List<LineItemModel>
                {
                    new LineItemModel
                    {
                        Price = 4.0M
                    }
                }
            });
        }

        [Test]
        public void Should_Not_Have_Error_When_Total_Is_Valid()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.Total, new OrderModel
            {
                Total = 10.0M,
                LineItems = new List<LineItemModel>
                {
                    new LineItemModel
                    {
                        Price = 10.0M
                    }
                }
            });
        }

        [Test]
        public void Should_Have_Error_When_LineItems_Null()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.LineItems, new OrderModel
            {
                LineItems = null
            });
        }

        [Test]
        public void Should_Have_Error_When_LineItems_Empty()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.LineItems, new OrderModel
            {
                LineItems = new List<LineItemModel>()
            });
        }
    }
}
