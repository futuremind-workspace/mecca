﻿using FluentAssertions;
using FluentValidation.TestHelper;
using MECCA.DomainLogic.Order.Models;
using MECCA.DomainLogic.Order.Validators;
using MECCA.DomainLogic.Test.Order.Models;
using NUnit.Framework;

namespace MECCA.DomainLogic.Test.Order.Validators
{
    [TestFixture]
    public class LineItemValidatorTest
    {
        private LineItemValidator _validator;

        [OneTimeSetUp]
        public void SetUp()
        {
            _validator = new LineItemValidator();
        }

        [Test]
        public void Should_Not_Have_Validation_Errors_When_ValidModel_Supplied()
        {
            var model = (new LineItemModelTest()).Model();
            _validator.Validate(model).IsValid.Should().BeTrue();
        }

        [Test]
        public void Should_Have_Error_When_ItemCode_Is_Empty()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.ItemCode, new LineItemModel
            {
                ItemCode = null
            });
        }

        [Test]
        public void Should_Not_Have_Error_When_ItemCode_Is_Empty()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.ItemCode, new LineItemModel
            {
                ItemCode = "TestItemCode"
            });
        }

        [Test]
        public void Should_Have_Error_When_Quantity_Is_Negative()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.Quantity, new LineItemModel
            {
                Quantity = -3
            });
        }

        [Test]
        public void Should_Not_Have_Error_When_Quantity_Is_Positive()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.Quantity, new LineItemModel
            {
                Quantity = 3
            });
        }

        [Test]
        public void Should_Have_Error_When_Price_Is_Negative()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.Price, new LineItemModel
            {
                Price = -10.0M,
            });
        }

        [Test]
        public void Should_Not_Have_Error_Price_Is_Positive()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.Price, new LineItemModel
            {
                Price = 10.0M,
            });
        }
    }
}
