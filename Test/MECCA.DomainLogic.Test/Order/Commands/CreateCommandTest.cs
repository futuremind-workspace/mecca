﻿using MECCA.DataLayer.Context;
using MECCA.DataLayer.IRepository;
using MECCA.DataLayer.Repository;
using MECCA.DomainEntities.ValueObjects.Order;
using MECCA.DomainLogic.Order.Commands;
using MECCA.DomainLogic.Test.Order.Models;
using NUnit.Framework;

namespace MECCA.DomainLogic.Test.Order.Commands
{
    [TestFixture]
    public class CreateCommandTest
    {
        private AppDbContext _context;
        private IUnitOfWork _uow;
        private ICreateCommand _command;

        [SetUp]
        public void SetUp()
        {
            _context = new AppDbContext();
            _uow = new UnitOfWork(_context);
            _command = new CreateCommand(_uow);
        } 

        [Test]
        public void CreateCommandTest_Submit_Should_Create_New_Order()
        {
            var model = (new OrderModelTest()).Model(OrderType.Online);

            var result = _command.Submit(model);

            Assert.IsTrue(result.ValidationResult.IsValid);
        }
    }
}
