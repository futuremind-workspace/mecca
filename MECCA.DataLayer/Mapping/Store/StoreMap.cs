﻿using System.Data.Entity.ModelConfiguration;

namespace MECCA.DataLayer.Mapping.Store
{
    public class StoreMap : EntityTypeConfiguration<DomainEntities.Entities.Store.Store>
    {
        public StoreMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            //properties

            // Table & Column Mappings
            this.ToTable("Store");
        }
    }
}
