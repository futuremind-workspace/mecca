﻿using System.Data.Entity.ModelConfiguration;

namespace MECCA.DataLayer.Mapping.Channel
{
    public class ChannelMap : EntityTypeConfiguration<DomainEntities.Entities.Channel.Channel>
    {
        public ChannelMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            //properties

            // Table & Column Mappings
            this.ToTable("Channel");
        }
    }
}
