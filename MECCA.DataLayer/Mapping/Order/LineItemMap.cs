﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MECCA.DataLayer.Mapping.Order
{
    public class LineItemMap : EntityTypeConfiguration<DomainEntities.Entities.Order.LineItem>
    {
        public LineItemMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            //properties
            this.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Table & Column Mappings
            this.ToTable("LineItem");

            // Relationships
            this.HasRequired(t => t.Order)
                .WithMany(t => t.LineItems)
                .HasForeignKey(d => d.OrderId);
        }
    }
}
