﻿using System.Data.Entity.ModelConfiguration;

namespace MECCA.DataLayer.Mapping.Order
{
    public class OrderStatusMap : EntityTypeConfiguration<DomainEntities.Entities.Order.OrderStatus>
    {
        public OrderStatusMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            //properties
            
            // Table & Column Mappings
            this.ToTable("OrderStatus");
        }
    }
}
