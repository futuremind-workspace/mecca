﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MECCA.DataLayer.Mapping.Order
{
    public class OrderMap : EntityTypeConfiguration<DomainEntities.Entities.Order.Order>
    {
        public OrderMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            //properties
            this.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Table & Column Mappings
            this.ToTable("Order");

            // Relationships
            this.HasOptional(t => t.Channel)
                .WithMany(t => t.Orders)
                .HasForeignKey(d => d.ChannelId);

            this.HasRequired(t => t.OrderStatus)
                .WithMany(t => t.Orders)
                .HasForeignKey(d => d.OrderStatusId);

            this.HasOptional(t => t.Store)
                .WithMany(t => t.Orders)
                .HasForeignKey(d => d.StoreId);
        }
    }
}
