﻿using System;
using System.Data.Entity;

namespace MECCA.DataLayer.IRepository
{
    public interface IUnitOfWork
    {
        IGenericRepository<T> GenericRepository<T>() where T : class;
        DbContextTransaction BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
        Int64 SaveChanges();
    }
}