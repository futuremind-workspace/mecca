﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace MECCA.DataLayer.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        List<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes);
        IQueryable<T> Query(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
        T FindById(object id);
        IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression);
        int Count(Expression<Func<T, bool>> searchExpression);
        IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy,
            out int totalResults);
        IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy,
            SortOrder order);
        IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy,
            out int totalResults, int? pageIndex = 1, int? pageSize = 100, SortOrder order = SortOrder.Ascending);
        IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy,
            out int totalResults, int maxRecords, int? pageIndex, int? pageSize, SortOrder order = SortOrder.Ascending);
        IQueryable<T> FindAsQueryable(Expression<Func<T, bool>> searchExpression);
        IQueryable<T> FindAsQueryable(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy,
            out int totalResults, int? pageIndex, int? pageSize, SortOrder order = SortOrder.Ascending);
        bool Any(Expression<Func<T, bool>> searchExpression);
        T GetFirstOrDefault(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes);
        T GetFirstOrDefaultAsNoTracking(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes);
        T GetSingle(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}