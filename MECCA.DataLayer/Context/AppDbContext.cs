﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using MECCA.DataLayer.Mapping.Channel;
using MECCA.DataLayer.Mapping.Order;
using MECCA.DataLayer.Mapping.Store;

namespace MECCA.DataLayer.Context
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        static AppDbContext()
        {
            Database.SetInitializer<AppDbContext>(null);
        }

        public AppDbContext() : base("AppDbContext")
        {
        }

        public static AppDbContext Init()
        {
            return new AppDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ChannelMap());
            modelBuilder.Configurations.Add(new LineItemMap());
            modelBuilder.Configurations.Add(new OrderMap());
            modelBuilder.Configurations.Add(new OrderStatusMap());
            modelBuilder.Configurations.Add(new StoreMap());
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var errorDetail = new StringBuilder();

                foreach (var validationError in ex.EntityValidationErrors.SelectMany(error => error.ValidationErrors))
                {
                    errorDetail.Append($"{validationError.PropertyName}, {validationError.ErrorMessage}{Environment.NewLine}");
                }

                throw new Exception(errorDetail.ToString(), ex);
            }
        }
    }
}
