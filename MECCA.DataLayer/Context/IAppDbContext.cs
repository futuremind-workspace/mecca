﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MECCA.DataLayer.Context
{
    public interface IAppDbContext : IDisposable
    {
        DbEntityEntry Entry(object entity);
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
        Database Database { get; }
    }
}
