﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using MECCA.Core.Expresssion;
using MECCA.DataLayer.Context;
using MECCA.DataLayer.IRepository;
using Z.EntityFramework.Plus;

namespace MECCA.DataLayer.Repository
{
    public class GenericRepository<T> : IDisposable, IGenericRepository<T> where T : class
    {
        private readonly IAppDbContext _context;
        private readonly DbSet<T> _dbSet;
        private bool disposed = false;

        public GenericRepository(IAppDbContext context)
        {
            this._context = context;
            _dbSet = context.Set<T>();
        }

        public virtual List<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _dbSet;

            foreach (Expression<Func<T, object>> include in includes)
                query = query.Include(include);

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query.ToList();
        }

        public virtual IQueryable<T> Query(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query;
        }

        public virtual T FindById(object id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression)
        {
            return _context.Set<T>().Where(searchExpression);
        }

        public int Count(Expression<Func<T, bool>> searchExpression)
        {
            return _context.Set<T>().Count(searchExpression);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy, out int totalResults)
        {
            IQueryable<T> results = _context.Set<T>().Where(searchExpression).ObjectSort(orderBy);
            totalResults = results.Count();
            return results;
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy, SortOrder order)
        {
            return _context.Set<T>().Where(searchExpression).ObjectSort(orderBy, order);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy, out int totalResults, int? pageIndex, int? pageSize, SortOrder order = SortOrder.Ascending)
        {
            IQueryable<T> results = _context.Set<T>().Where(searchExpression);
            totalResults = results.Count();

            if (pageIndex == null || pageSize == null)
            {
                return results.ObjectSort(orderBy, order);
            }

            return results.ObjectSort(orderBy, order).Skip(((pageIndex ?? 1) - 1) * (pageSize ?? 100)).Take(pageSize ?? 100);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy, out int totalResults, int maxRecords, int? pageIndex, int? pageSize, SortOrder order = SortOrder.Ascending)
        {
            IQueryable<T> results = _context.Set<T>().Where(searchExpression);
            totalResults = results.Count();

            if (pageIndex == null || pageSize == null)
            {
                return results.ObjectSort(orderBy, order).Take(maxRecords);
            }

            return results.ObjectSort(orderBy, order).Take(maxRecords).Skip(((pageIndex ?? 1) - 1) * (pageSize ?? 100)).Take(pageSize ?? 100); ;
        }

        public IQueryable<T> FindAsQueryable(Expression<Func<T, bool>> searchExpression)
        {
            return _context.Set<T>().Where(searchExpression);
        }

        public IQueryable<T> FindAsQueryable(Expression<Func<T, bool>> searchExpression, Expression<Func<T, object>> orderBy, out int totalResults, int? pageIndex, int? pageSize, SortOrder order = SortOrder.Ascending)
        {
            IQueryable<T> results = _context.Set<T>().Where(searchExpression);
            totalResults = results.Count();

            if (pageIndex == null || pageSize == null)
            {
                return results.ObjectSort(orderBy, order);
            }

            return results.ObjectSort(orderBy, order).Skip(((pageIndex ?? 1) - 1) * (pageSize ?? 100)).Take(pageSize ?? 100);
        }

        public bool Any(Expression<Func<T, bool>> searchExpression)
        {
            return _context.Set<T>().Any(searchExpression);
        }

        public virtual T GetFirstOrDefault(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _dbSet;

            foreach (Expression<Func<T, object>> include in includes)
            {
                //IncludeFilter - http://entityframework-plus.net/ feature
                query = query.IncludeFilter(include);
            }
            
            if (filter == null)
            {
                filter = f => true;
            }

            return query.FirstOrDefault(filter);
        }

        public virtual T GetFirstOrDefaultAsNoTracking(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _dbSet.AsNoTracking();

            foreach (Expression<Func<T, object>> include in includes)
            {
                //IncludeFilter - http://entityframework-plus.net/ feature
                query = query.IncludeFilter(include);
            }

            if (filter == null)
            {
                filter = f => true;
            }

            return query.FirstOrDefault(filter);
        }

        public virtual T GetSingle(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _dbSet;
            foreach (Expression<Func<T, object>> include in includes)
            {
                //IncludeFilter - http://entityframework-plus.net/ feature
                query = query.IncludeFilter(include);
            }
            
            if (filter == null)
            {
                filter = f => true;
            }

            return query.Single(filter);
        }

        public virtual void Add(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                _dbSet.Add(entity);
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        msg += string.Format("Property: {0} Error: {1}",
                                   validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }

                var fail = new Exception(msg, dbEx);
                throw fail;
            }
        }

        public virtual void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
