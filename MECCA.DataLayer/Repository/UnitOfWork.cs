﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MECCA.DataLayer.Context;
using MECCA.DataLayer.IRepository;

namespace MECCA.DataLayer.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly IAppDbContext _context;
        public Dictionary<Type, object> repositories = new Dictionary<Type, object>();
        private DbContextTransaction _transaction;
        private bool disposed = false;

        public UnitOfWork(IAppDbContext context)
        {
            this._context = context;
        }

        public IGenericRepository<T> GenericRepository<T>() where T : class
        {
            if (repositories.Keys.Contains(typeof(T)) == true)
            {
                return repositories[typeof(T)] as IGenericRepository<T>;
            }
            IGenericRepository<T> repo = new GenericRepository<T>(_context);
            repositories.Add(typeof(T), repo);
            return repo;
        }

        public DbContextTransaction BeginTransaction()
        {
            _transaction = _context.Database.BeginTransaction();
            return _transaction;
        }

        public void CommitTransaction()
        {
            _transaction.Commit();
        }

        public void RollbackTransaction()
        {
            _transaction.Rollback();
        }

        public Int64 SaveChanges()
        {
            return _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}