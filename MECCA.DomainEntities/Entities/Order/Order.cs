﻿using System;
using System.Collections.Generic;
using MECCA.DomainEntities.Entities.Shared;

namespace MECCA.DomainEntities.Entities.Order
{
    public class Order
    {
        public Guid Id { get; set; }
        public string OrderNumber { get; set; }
        public DateTime  OrderDate { get; set; }
        public int OrderStatusId { get; set; }
        public decimal Total { get; set; }
        public int? ChannelId { get; set; }
        public int? StoreId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public virtual Channel.Channel Channel { get; set; }
        public virtual Store.Store Store { get; set; }
        public virtual OrderStatus OrderStatus { get; set; }
        public virtual List<LineItem> LineItems { get; set; }
    }
}
