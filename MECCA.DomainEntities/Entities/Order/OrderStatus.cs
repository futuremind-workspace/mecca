﻿using System.Collections.Generic;
using MECCA.DomainEntities.Entities.Shared;

namespace MECCA.DomainEntities.Entities.Order
{
    public class OrderStatus :Entity
    {
        public string Name { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
