﻿using System;
using MECCA.DomainEntities.Entities.Shared;

namespace MECCA.DomainEntities.Entities.Order
{
    public class LineItem
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public string  ItemCode { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public virtual Order Order { get; set; }
    }
}
