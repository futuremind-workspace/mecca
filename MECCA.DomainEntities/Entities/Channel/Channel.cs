﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using MECCA.DomainEntities.Entities.Shared;

namespace MECCA.DomainEntities.Entities.Channel
{
    public class Channel : Entity
    {
        public string Name { get; set; }
        public virtual Collection<Order.Order> Orders { get; set; }
    }
}
