﻿using System;
using System.Collections.Generic;
using MECCA.DomainEntities.Entities.Shared;

namespace MECCA.DomainEntities.Entities.Store
{
    public class Store : Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string InternalCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public virtual ICollection<Order.Order> Orders { get; set; }
    }
}
