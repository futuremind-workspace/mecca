﻿namespace MECCA.DomainEntities.ValueObjects.Order
{
    public enum OrderType
    {
        Online = 1,
        Store = 2
    }
}
