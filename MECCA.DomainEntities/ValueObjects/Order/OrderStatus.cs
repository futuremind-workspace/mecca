﻿namespace MECCA.DomainEntities.ValueObjects.Order
{
    public enum OrderStatus
    {
        New = 1,
        Pending = 2,
        Complete = 3
    }
}
