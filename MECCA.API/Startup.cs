﻿using System.Web.Http;
using Microsoft.Owin;
using Owin;
using Swashbuckle.Application;

[assembly: OwinStartup(typeof(MECCA.Full.AspNet.API.Startup))]

namespace MECCA.Full.AspNet.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = GetInjectionConfiguration();
            WebApiConfig.Register(configuration);
            app.UseWebApi(configuration);
        }

        public virtual HttpConfiguration GetInjectionConfiguration()
        {
            var configuration = new HttpConfiguration();



            return configuration;
        }
    }
}
