﻿using System;
using System.Web.Http;
using FluentValidation.WebApi;
using MECCA.DomainLogic.Order.Commands;
using MECCA.DomainLogic.Order.Models;
using MECCA.DomainLogic.Order.Queries;

namespace MECCA.Full.AspNet.API.Controllers
{
    [System.Web.Http.RoutePrefix("api/order")]
    public class OrderController : BaseApiController
    {
        private readonly ISearchQuery _searchQuery;
        private readonly ICreateCommand _createCommand;

        public OrderController(ISearchQuery searchQuery, ICreateCommand createCommand)
        {
            _searchQuery = searchQuery;
            _createCommand = createCommand;
        }

        [HttpPost]
        [Route("submit")]
        public IHttpActionResult Submit(OrderModel model)
        {
            try
            {
                var result = _createCommand.Submit(model);
                result.ValidationResult.AddToModelState(ModelState, null);

                if (ModelState.IsValid)
                {
                    return Ok();
                }

                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return HandleError(ex);
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("search/{orderNumber}")]
        public IHttpActionResult Search(string orderNumber)
        {
            try
            {
                return Ok(_searchQuery.Search(orderNumber));
            }
            catch (Exception ex)
            {
                return HandleError(ex);
            }
        }
    }
}
