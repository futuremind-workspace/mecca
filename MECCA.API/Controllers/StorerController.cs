﻿using System;
using System.Web.Http;
using MECCA.DomainLogic.Store.Queries;

namespace MECCA.Full.AspNet.API.Controllers
{
    [System.Web.Http.RoutePrefix("api/store")]
    public class StoreController : BaseApiController
    {
        private readonly ISearchQuery _searchQuery;

        public StoreController(ISearchQuery searchQuery)
        {
            _searchQuery = searchQuery;
        }

        [HttpGet]
        [Route("search")]
        public IHttpActionResult Search()
        {
            try
            {
                return Ok(_searchQuery.Search());
            }
            catch (Exception ex)
            {
                return HandleError(ex);
            }
        }
    }
}
