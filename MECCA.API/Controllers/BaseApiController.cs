﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;
using Elmah;

namespace MECCA.Full.AspNet.API.Controllers
{
    public class BaseApiController : ApiController
    {
        protected IHttpActionResult HandleError(Exception ex)
        {
            // Log error with ELMAH
            ErrorSignal.FromCurrentContext().Raise(ex);

            if (ex is ValidationException)
            {
                return BadRequest(ex.Message);
            }

            return BadRequest("An error occured. Please contact support");
        }
    }
}