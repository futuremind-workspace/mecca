﻿using System;
using System.Collections.Generic;
using System.Linq;
using MECCA.DataLayer.IRepository;
using MECCA.DomainEntities.Entities.Order;
using MECCA.DomainLogic.Order.Models;
using MECCA.DomainLogic.Order.Validators;

namespace MECCA.DomainLogic.Order.Commands
{
    public class CreateCommand : ICreateCommand
    {
        private readonly IUnitOfWork _uow;

        public CreateCommand(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public OrderModel Submit(OrderModel model)
        {
            var validationResult = new OrderValidator().Validate(model);

            if (validationResult.IsValid)
            {
                DomainEntities.Entities.Order.Order order = new DomainEntities.Entities.Order.Order();

                order.Id = Guid.NewGuid();
                order.OrderNumber = model.OrderNumber;
                order.OrderDate = model.OrderDate;

                int statusId = (int) DomainEntities.ValueObjects.Order.OrderStatus.New;
                order.OrderStatus = _uow.GenericRepository<OrderStatus>().FindById(statusId);

                order.Total = model.Total;
                order.ChannelId = model.ChannelId;
                order.StoreId = model.StoreId;
                order.CreatedDate = DateTime.UtcNow;
                order.LastModifiedDate = null;

                order.LineItems = new List<LineItem>
                (
                    model.LineItems.Select
                    (
                        i => new LineItem
                        {
                            Id = Guid.NewGuid(),
                            ItemCode = i.ItemCode,
                            Quantity = i.Quantity,
                            Price = i.Price,
                            CreatedDate = DateTime.UtcNow,
                            LastModifiedDate = null
                        }
                    )
                );

                _uow.GenericRepository<DomainEntities.Entities.Order.Order>().Add(order);
                _uow.SaveChanges();
            }

            model.ValidationResult = validationResult;
            return model;
        }
    }
}
