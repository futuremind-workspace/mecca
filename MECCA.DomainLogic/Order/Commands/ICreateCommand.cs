﻿using MECCA.DomainLogic.Order.Models;

namespace MECCA.DomainLogic.Order.Commands
{
    public interface ICreateCommand
    {
        OrderModel Submit(OrderModel model);
    }
}