﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MECCA.DomainLogic.Order.Models;

namespace MECCA.DomainLogic.Order.Queries
{
    public interface ISearchQuery
    {
        OrderModel Search(string orderNumber);
    }
}
