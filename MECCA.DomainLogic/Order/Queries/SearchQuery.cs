﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MECCA.Core.Expresssion;
using MECCA.DataLayer.IRepository;
using MECCA.DomainLogic.Order.Models;

namespace MECCA.DomainLogic.Order.Queries
{
    public class SearchQuery : ISearchQuery
    {
        private readonly IUnitOfWork _uow;

        public SearchQuery(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public OrderModel Search(string orderNumber)
        {
            var order = _uow.GenericRepository<DomainEntities.Entities.Order.Order>()
                .Find(GetSearchExpression(orderNumber), GetOrderbyExpression(), out var totalWIRecords, 1, 10)
                .FirstOrDefault();

            if (order == null)
            {
                return null;
            }

            return new OrderModel
            {
                Total = order.Total
            };
        }

        private Expression<Func<DomainEntities.Entities.Order.Order, bool>> GetSearchExpression(string orderNumber)
        {
            Expression<Func<DomainEntities.Entities.Order.Order, bool>> qry = q => true;

            if (string.IsNullOrEmpty(orderNumber))
            {
                qry = qry.And(o => o.OrderNumber.Contains(orderNumber));
            }

            return qry;
        }

        private Expression<Func<DomainEntities.Entities.Order.Order, object>> GetOrderbyExpression()
        {
            return x => x.Id;
        }
    }
}
