﻿using System;

namespace MECCA.DomainLogic.Order.Models
{
    public class LineItemModel
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public string  ItemCode { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
