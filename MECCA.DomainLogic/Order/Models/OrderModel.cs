﻿using System;
using System.Collections.Generic;
using MECCA.DomainEntities.ValueObjects.Order;
using MECCA.DomainLogic.Shared.Models;

namespace MECCA.DomainLogic.Order.Models
{
    public class OrderModel : ErrorResultModel
    {
        public Guid Id { get; set; }
        public string OrderNumber { get; set; }
        public DateTime  OrderDate { get; set; }
        public decimal Total { get; set; }
        public int? ChannelId { get; set; }
        public int? StoreId { get; set; }
        public OrderType OrderType { get; set; }
        public List<LineItemModel> LineItems { get; set; }
    }
}
