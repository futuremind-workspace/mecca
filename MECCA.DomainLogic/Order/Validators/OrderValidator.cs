﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using MECCA.DomainEntities.ValueObjects.Order;
using MECCA.DomainLogic.Order.Models;

namespace MECCA.DomainLogic.Order.Validators
{
    public class OrderValidator : AbstractValidator<OrderModel>
    {
        public OrderValidator()
        {
            RuleFor(i => i.OrderNumber)
                .NotNull()
                .WithMessage("Order number cannot be null");

            RuleFor(i => i.OrderDate)
                .Must((m,o) => BeAValidDate(m.OrderDate))
                .WithMessage("Invalid Order date");

            RuleFor(i => i.ChannelId)
                .NotNull()
                .When(i => i.OrderType == OrderType.Online)
                .WithMessage("Channel Id cannot be null");

            RuleFor(i => i.StoreId)
                .NotNull()
                .When(i => i.OrderType == OrderType.Store)
                .WithMessage("Store Id cannot be null");

            RuleFor(i => i.Total)
                .Must((m,o) => BeAValidTotal(m.Total, m.LineItems))
                .WithMessage("Invalid total");

            RuleFor(i => i.LineItems)
                .NotNull()
                .WithMessage("Invalid line items");

            RuleFor(i => i.LineItems)
                .Must(i => i?.Count > 0)
                .When(i => i != null)
                .WithMessage("Invalid line items");

            RuleFor(i => i.LineItems).SetCollectionValidator(new LineItemValidator()).When(i => i?.LineItems != null);
                
        }

        private bool BeAValidDate(DateTime date)
        {
            return date < DateTime.UtcNow.AddDays(1);
        }

        private bool BeAValidTotal(decimal total, List<LineItemModel> lineItems)
        {
            return total == lineItems?.Sum(i => i.Price);
        }
    }
}
