﻿using FluentValidation;
using MECCA.DomainLogic.Order.Models;

namespace MECCA.DomainLogic.Order.Validators
{
    public class LineItemValidator : AbstractValidator<LineItemModel>
    {
        public LineItemValidator()
        {
            RuleFor(i => i.ItemCode)
                .NotNull()
                .WithMessage("Item code cannot be null");

            RuleFor(i => i.Quantity)
                .Must(i => i > 0)
                .WithMessage("Invalid quantity");

            RuleFor(i => i.Price)
                .Must(i => i > 0)
                .WithMessage("Invalid price");
        }
    }
}
