﻿using MECCA.DomainLogic.Shared.Models;

namespace MECCA.DomainLogic.Store.Models
{
    public class StoreModel 
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string InternalCode { get; set; }
    }
}
