﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MECCA.DataLayer.IRepository;
using MECCA.DomainLogic.Store.Models;

namespace MECCA.DomainLogic.Store.Queries
{
    public class SearchQuery : ISearchQuery
    {
        private readonly IUnitOfWork _uow;

        public SearchQuery(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public List<StoreModel> Search()
        {
            var storeList = _uow.GenericRepository<DomainEntities.Entities.Store.Store>()
                .Find(GetSearchExpression(), GetOrderbyExpression(), out var totalWIRecords, 1, 10)
                .ToList();

            if (!storeList.Any())
            {
                return null;
            }

            //TODO: Use Automapper
            return storeList.Select(s => new StoreModel
            {
                Code = s.Code
            }).ToList();
        }

        private Expression<Func<DomainEntities.Entities.Store.Store, bool>> GetSearchExpression()
        {
            Expression<Func<DomainEntities.Entities.Store.Store, bool>> qry = q => true;

            return qry;
        }

        private Expression<Func<DomainEntities.Entities.Store.Store, object>> GetOrderbyExpression()
        {
            return x => x.Id;
        }
    }
}
