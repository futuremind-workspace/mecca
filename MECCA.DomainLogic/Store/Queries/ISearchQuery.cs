﻿using System.Collections.Generic;
using MECCA.DomainLogic.Order.Models;
using MECCA.DomainLogic.Store.Models;

namespace MECCA.DomainLogic.Store.Queries
{
    public interface ISearchQuery
    {
        List<StoreModel> Search();
    }
}
