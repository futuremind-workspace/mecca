﻿using FluentValidation.Results;

namespace MECCA.DomainLogic.Shared.Models
{
    public class ErrorResultModel
    {
        public ValidationResult ValidationResult { get; set; }
    }
}
