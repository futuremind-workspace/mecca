﻿CREATE TABLE [dbo].[Order] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_Order_Id] DEFAULT (newsequentialid()) NOT NULL,
    [OrderNumber]      VARCHAR (20)     NOT NULL,
    [OrderDate]        DATETIME         NOT NULL,
    [OrderStatusId]    INT              NOT NULL,
    [Total]            DECIMAL (18, 2)  NOT NULL,
    [ChannelId]        INT              NULL,
    [StoreId]          INT              NULL,
    [CreatedDate]      DATETIME         CONSTRAINT [DF_Order_CreatedDate] DEFAULT (sysutcdatetime()) NOT NULL,
    [LastModifiedDate] DATETIME         NULL,
    CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Order_Channel] FOREIGN KEY ([ChannelId]) REFERENCES [dbo].[Channel] ([Id]),
    CONSTRAINT [FK_Order_OrderStatus] FOREIGN KEY ([OrderStatusId]) REFERENCES [dbo].[OrderStatus] ([Id]),
    CONSTRAINT [FK_Order_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Order_OrderNumber]
    ON [dbo].[Order]([OrderNumber] ASC);

