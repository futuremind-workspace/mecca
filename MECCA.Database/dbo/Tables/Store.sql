﻿CREATE TABLE [dbo].[Store] (
    [Id]               INT          IDENTITY (1, 1) NOT NULL,
    [Code]             VARCHAR (5)  NOT NULL,
    [Name]             VARCHAR (50) NOT NULL,
    [InternalCode]     VARCHAR (10) NOT NULL,
    [CreatedDate]      DATETIME     CONSTRAINT [DF_Store_CreatedDate] DEFAULT (sysutcdatetime()) NOT NULL,
    [LastModifiedDate] DATETIME     NULL,
    CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED ([Id] ASC)
);

