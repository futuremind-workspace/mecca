﻿CREATE TABLE [dbo].[LineItem] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_LineItem_Id] DEFAULT (newsequentialid()) NOT NULL,
    [OrderId]          UNIQUEIDENTIFIER NOT NULL,
    [ItemCode]         VARCHAR (50)     NOT NULL,
    [Quantity]         INT              NOT NULL,
    [Price]            DECIMAL (18, 2)  NOT NULL,
    [CreatedDate]      DATETIME         CONSTRAINT [DF_LineItem_CreatedDate] DEFAULT (sysutcdatetime()) NOT NULL,
    [LastModifiedDate] DATETIME         NULL,
    CONSTRAINT [PK_LineItem] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LineItem_Order] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Order] ([Id])
);

